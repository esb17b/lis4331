# LIS 4331 Mobile Application Development

## Elijah Butts

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create Contacts App
- Provide Screenshots of Installations 
    - Create Bitbucket repos
    - Complete Bitbucket tutourials 
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
  	- Create TipCalculator
      	- Meet Specifications 
            	- Provide Screenshots 
         	- Splash Screen
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create CurrencyConverter
    - Meet Specifications
    - Provide Screenshots 
    - Toast Message
    - Splash Screen
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
   
    - Create MortgageCalculator
    - Meet Specifications
    - Provide Screenshots 
    - Error Message
    - Splash Screen
    - Correct Run
    
    [A5 README.md](a5/README.md "My A5 README.md file")
    
    - Create News Reader
    - Meet Specifications
    - Provide Screenshots 
    - Splash Screen With Articles
    - Link to Browser
    - Article Decription Screenshot
    
5. [P1 README.md](p1/README.md "My A3 README.md file")
    - Create Music Application
    - Meet Specifications
    - Include splash screen image, app title, intro text 
    - Include artists’ images and media.
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color(s) or theme
5. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create Task List Application
    - Meet Specifications
    - Include splash screen image, app title, intro text 
    - Use Sql Database.
    - Five Sample Tasks.
    - Must add background color(s) or theme