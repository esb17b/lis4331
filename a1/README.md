## Elijah Butts

### Assignment 1 # Requirements:

*Sub-Heading:*

1. Java and JDK download
2. Android Dev Studio Install
3. Push your localrepository to the one hosted by Bitbucket's servers
4. Provide me with read-onlyaccess to Bitbucket repository

#### README.md file Includes:

* Screenshor of Java running Hello World
* Screenshot of running First App
* Screenshot of Android studio running Contacts
* Git commands with description

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

![App Screen Shot 1](img/FirstScreen.png)

![App Screen Shot 2](img/SecondScreen.png)

*Screenshot of running java Hello*:

![Java Hello Screen Shot](img/JavaHello.png)

*Screenshot of Android Studio - My First App*:

![Android Development Studio](img/Android.png)

