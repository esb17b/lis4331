## Elijah Butts

### Assignment 2 # Requirements:

*Sub-Heading:*

1. Create TipCalculator
2. Meet Specificationsl
3. Provide Screenshots
4. Splash Screen

#### README.md file Includes:

* Screenshots of Application
* Screenshot of Splash Screen 
* Screenshot of Android studio running TipCalculator

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

| ![App Screen Shot 1](img/FirstScreen.png)  | ![App Screen Shot 2](img/SecondScreen.png) |
| ------------------------------------------ | ------------------------------------------ |
| ![Splash Screen img](img/SplashScreen.png) |                                            |

*Screenshot of Android Studio - Contacts*:

![Android Development Studio](img/Android.png)

