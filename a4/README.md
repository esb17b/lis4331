## Elijah Butts

### Assignment 4 # Requirements:

*Sub-Heading:*

1. Create Mortgage Calculator
2. Meet Specificationsl
3. Provide Screenshots
4. Splash Screen
5. Error message

#### README.md file Includes:

* Screenshots of Application
* Screenshot of Error  Message
* Screenshot of Splash Screen 
* Screenshot of Correct Run

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

| ![App Screen Shot 1](img/FirstScreen.png)  | ![App Screen Shot 2](img/SecondScreen.png)     |
| ------------------------------------------ | ---------------------------------------------- |
| ![Splash Screen img](img/SplashScreen.png) | ![Android Development Studio](img/Android.png) |

