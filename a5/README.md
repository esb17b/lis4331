## Elijah Butts

### Assignment 5 # Requirements:

*Sub-Heading:*

1. Create News Reader
2. Meet Specifications
3. Provide Screenshots
4. Splash Screen With Articles
5. Link to browser

#### README.md file Includes:

* Screenshots of Application
* Screenshot of Browser
* Screenshot of Articles 
* Screenshot of Article Description

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

| ![App Screen Shot 1](img/FirstScreen.png)  | ![App Screen Shot 2](img/SecondScreen.png)     |
| ------------------------------------------ | ---------------------------------------------- |
| ![Splash Screen img](img/SplashScreen.png) | ![Android Development Studio](img/Android.png) |

