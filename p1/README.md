## Elijah Butts

### Project 1 # Requirements:

*Sub-Heading:*

1. Create Music Application
2. Meet Specificationsl
3. Provide Screenshots
4. Splash Screen
5. Play and Pause Interface

#### README.md file Includes:

* Screenshots of Application
* Screenshot of Splash Screen 
* Screenshot of Followup Screen
* Screenshot of Play and Pause interfaces

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

| ![App Screen Shot 1](img/FirstScreen.png) | ![App Screen Shot 2](img/SecondScreen.png)          |
| ----------------------------------------- | --------------------------------------------------- |
| ![Splash Screen img](img/Android.png)     | ![Android Development Studio](img/SplashScreen.png) |

