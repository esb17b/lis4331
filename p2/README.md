## Elijah Butts

### Project 2 # Requirements:

*Sub-Heading:*

1. Create Task List Application
2. Meet Specifications
3. Provide Screenshots
4. Splash Screen
5. Use Sql Database

#### README.md file Includes:

* Screenshots of Application
* Screenshot of Splash Screen 
* Screenshot of List of Tasks
* Screenshot of items Persisting Upon App Close

> #### Bitbucket Links:

LIS 4331: https://bitbucket.org/esb17b/lis4331/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/



#### Assignment Screenshots:

*Screenshot of APP running*:

| ![App Screen Shot 1](img/FirstScreen.png) | ![App Screen Shot 2](img/SecondScreen.png)          |
| ----------------------------------------- | --------------------------------------------------- |
| ![Splash Screen img](img/Android.png)     | ![Android Development Studio](img/SplashScreen.png) |

